
package api;

import api.ferreteria.modelo.Boleta;
import api.ferreteria.modelo.DetalleBoleta;
import api.ferreteria.modelo.StockProducto;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/boleta")
public class APIBoleta {
    
    @Inject EntityManager manager;
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta postBoleta(Boleta boletaIn){
        
        manager.persist(boletaIn);
        manager.flush();
        
        Boleta newBoleta = manager.find(Boleta.class, boletaIn.getIdboleta());
        
        for(DetalleBoleta detalle : boletaIn.getDetallesboleta()){
            detalle.setBoleta(newBoleta);
            manager.persist(detalle);
            manager.flush();
        }
        
        return manager.find(Boleta.class, boletaIn.getIdboleta());
    }
    
    @PATCH
    @Path("/{idboleta}/aprobar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta aprobarBoleta(Boleta boleta){
        //Se crea una nueva clase boleta con la boleta que viene
        //Para prevenir el problema de los DetalleBoleta asociados
        Boleta newBoleta = new Boleta();
        newBoleta.setCliente(boleta.getCliente());
        newBoleta.setEmpleado(boleta.getEmpleado());
        newBoleta.setEstadoboleta(boleta.getEstadoboleta());
        newBoleta.setFechaestado(boleta.getFechaestado());
        newBoleta.setIdboleta(boleta.getIdboleta());
        newBoleta.setTotalbruto(boleta.getTotalbruto());
        newBoleta.setTotaliva(boleta.getTotaliva());
        newBoleta.setTotalneto(boleta.getTotalneto());
        
        manager.merge(newBoleta);
        manager.flush();
        
        //Luego de aprobar, se deben descontar todos los stock
        for(DetalleBoleta db : boleta.getDetallesboleta()){
            StockProducto sp = manager.find(StockProducto.class, db.getStockproducto().getIdstockproducto());
            
            //Para evitarnos problemas, en caso de que el stock pase a valores negativos, los dejamos en cero
            int restante = sp.getStock() - db.getCantidad();
            if(restante < 0){
                restante = 0;
            }
            sp.setStock(restante);
            manager.merge(sp);
            manager.flush();
        }
        return manager.find(Boleta.class, boleta.getIdboleta());
    }
    
    @PATCH
    @Path("/{idboleta}/rechazar")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta rechazarBoleta(Boleta boleta){
        
        Boleta newBoleta = new Boleta();
        newBoleta.setCliente(boleta.getCliente());
        newBoleta.setEmpleado(boleta.getEmpleado());
        newBoleta.setEstadoboleta(boleta.getEstadoboleta());
        newBoleta.setFechaestado(boleta.getFechaestado());
        newBoleta.setIdboleta(boleta.getIdboleta());
        newBoleta.setTotalbruto(boleta.getTotalbruto());
        newBoleta.setTotaliva(boleta.getTotaliva());
        newBoleta.setTotalneto(boleta.getTotalneto());
        
        manager.merge(newBoleta);
        manager.flush();
        return manager.find(Boleta.class, boleta.getIdboleta());
    }
}
