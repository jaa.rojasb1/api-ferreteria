package api.ferreteria.client;

import api.ferreteria.modelo.Boleta;
import java.util.Collection;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "config.ms.ferreteria")
@Path("boleta")
public interface ClientBoleta {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Boleta> getBoletas();
    
    @GET
    @Path("/{idboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boleta getBoleta(@PathParam("idboleta") Integer idboleta);
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta postBoleta(Boleta boleta);
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta patchBoleta(Boleta boleta);
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta deleteBoleta(Boleta boleta);
}
