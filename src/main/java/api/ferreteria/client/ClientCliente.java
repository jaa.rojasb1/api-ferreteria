package api.ferreteria.client;

import api.ferreteria.modelo.Cliente;
import java.util.Collection;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "config.ms.ferreteria")
@Path("cliente")
public interface ClientCliente {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Cliente> getClientes();
    
    @GET
    @Path("/{idcliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente getCliente(@PathParam("idcliente") Integer idcliente);
    
    @GET
    @Path("/rutcliente/{rutcliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Cliente getCliente(@PathParam("rutcliente") String rutcliente);
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente postCliente(Cliente cliente);
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente patchCliente(Cliente cliente);
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Cliente deleteCliente(Cliente cliente);
}
