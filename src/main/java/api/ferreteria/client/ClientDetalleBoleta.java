package api.ferreteria.client;

import api.ferreteria.modelo.DetalleBoleta;
import java.util.Collection;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "config.ms.ferreteria")
@Path("detalleboleta")
public interface ClientDetalleBoleta {

    @GET
    @Path("/{idboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<DetalleBoleta> getDetallesBoleta(@PathParam("idboleta") Integer idboleta);

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta postDetalleBoleta(DetalleBoleta detalleboleta);

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta patchDetalleBoleta(DetalleBoleta detalleboleta);

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta deleteDetalleBoleta(DetalleBoleta detalleboleta);
}
