package api.ferreteria.modelo;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@ApplicationScoped
@Entity(name = "BOLETA")
@Table(name = "BOLETA")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idboleta")
public class Boleta implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDBOLETA")
    private Integer idboleta;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDESTADOBOLETA")
    private EstadoBoleta estadoboleta;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDEMPLEADO")
    private Empleado empleado;

    @Column(name = "FECHAESTADO")
    private LocalDate fechaestado;

    @Column(name = "TOTALNETO")
    private Integer totalneto;
    
    @Column(name = "TOTALIVA")
    private Integer totaliva;
    
    @Column(name = "TOTALBRUTO")
    private Integer totalbruto;

    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDCLIENTE")
    private Cliente cliente;
    
    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "boleta")
    private Set<DetalleBoleta> detallesboleta = new HashSet<>();

    public Boleta() {
    }

    public Boleta(Integer idboleta, EstadoBoleta estadoboleta, Empleado empleado, LocalDate fechaestado, Integer totalneto, Integer totaliva, Integer totalbruto, Cliente cliente) {
        this.idboleta = idboleta;
        this.estadoboleta = estadoboleta;
        this.empleado = empleado;
        this.fechaestado = fechaestado;
        this.totalneto = totalneto;
        this.totaliva = totaliva;
        this.totalbruto = totalbruto;
        this.cliente = cliente;
    }

    public Integer getIdboleta() {
        return idboleta;
    }

    public void setIdboleta(Integer idboleta) {
        this.idboleta = idboleta;
    }

    public EstadoBoleta getEstadoboleta() {
        return estadoboleta;
    }

    public void setEstadoboleta(EstadoBoleta estadoboleta) {
        this.estadoboleta = estadoboleta;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    public LocalDate getFechaestado() {
        return fechaestado;
    }

    public void setFechaestado(LocalDate fechaestado) {
        this.fechaestado = fechaestado;
    }

    public Integer getTotalneto() {
        return totalneto;
    }

    public void setTotalneto(Integer totalneto) {
        this.totalneto = totalneto;
    }

    public Integer getTotaliva() {
        return totaliva;
    }

    public void setTotaliva(Integer totaliva) {
        this.totaliva = totaliva;
    }

    public Integer getTotalbruto() {
        return totalbruto;
    }

    public void setTotalbruto(Integer totalbruto) {
        this.totalbruto = totalbruto;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Set<DetalleBoleta> getDetallesboleta() {
        return detallesboleta;
    }

    public void setDetallesboleta(Set<DetalleBoleta> detallesboleta) {
        this.detallesboleta = detallesboleta;
    }

    
}
