/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "CLIENTE")
public class Cliente implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDCLIENTE")
    Integer idcliente;
    
    @Column(name = "RUTCLIENTE")
    String rutcliente;
    
    @Column(name = "NOMCLIENTE")
    String nomcliente;
    
    @Column(name = "APPATERNO")
    String appaterno;
    
    @Column(name = "APMATERNO")
    String apmaterno;
    
    @Column(name = "DIRECCIONPARTICULAR")
    String direccionparticular;
    
    @Column(name = "FONO")
    String fono;

    @Column(name = "EMAIL")
    String email;

    public Cliente() {
    }

    public Cliente(Integer idcliente, String rutcliente, String nomcliente, String appaterno, String apmaterno, String direccionparticular, String fono, String email) {
        this.idcliente = idcliente;
        this.rutcliente = rutcliente;
        this.nomcliente = nomcliente;
        this.appaterno = appaterno;
        this.apmaterno = apmaterno;
        this.direccionparticular = direccionparticular;
        this.fono = fono;
        this.email = email;
    }

    public Integer getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(Integer idcliente) {
        this.idcliente = idcliente;
    }

    public String getRutcliente() {
        return rutcliente;
    }

    public void setRutcliente(String rutcliente) {
        this.rutcliente = rutcliente;
    }

    public String getNomcliente() {
        return nomcliente;
    }

    public void setNomcliente(String nomcliente) {
        this.nomcliente = nomcliente;
    }

    public String getAppaterno() {
        return appaterno;
    }

    public void setAppaterno(String appaterno) {
        this.appaterno = appaterno;
    }

    public String getApmaterno() {
        return apmaterno;
    }

    public void setApmaterno(String apmaterno) {
        this.apmaterno = apmaterno;
    }

    public String getDireccionparticular() {
        return direccionparticular;
    }

    public void setDireccionparticular(String direccionparticular) {
        this.direccionparticular = direccionparticular;
    }

    public String getFono() {
        return fono;
    }

    public void setFono(String fono) {
        this.fono = fono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
    
    
    
}
