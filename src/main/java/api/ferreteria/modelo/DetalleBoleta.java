package api.ferreteria.modelo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "DETALLEBOLETA")
@ApplicationScoped
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "iddetalleboleta")
public class DetalleBoleta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDDETALLEBOLETA")
    private Integer iddetalleboleta;

    @JsonbTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDBOLETA", nullable = false)
    //@OnDelete(action = OnDeleteAction.CASCADE)
    @JsonBackReference
    private Boleta boleta;

    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDSTOCKPRODUCTO")
    private StockProducto stockproducto;

    @Column(name = "CANTIDAD")
    private Integer cantidad;

    @Column(name = "SUBTOTALNETO")
    private Integer subtotalneto;

    @Column(name = "SUBTOTALIVA")
    private Integer subtotaliva;

    @Column(name = "SUBTOTALBRUTO")
    private Integer subtotalbruto;

    public DetalleBoleta() {
    }

    public DetalleBoleta(Integer iddetalleboleta, Boleta boleta, StockProducto stockproducto, Integer cantidad, Integer subtotalneto, Integer subtotaliva, Integer subtotalbruto) {
        this.iddetalleboleta = iddetalleboleta;
        this.boleta = boleta;
        this.stockproducto = stockproducto;
        this.cantidad = cantidad;
        this.subtotalneto = subtotalneto;
        this.subtotaliva = subtotaliva;
        this.subtotalbruto = subtotalbruto;
    }

    public Integer getIddetalleboleta() {
        return iddetalleboleta;
    }

    public void setIddetalleboleta(Integer iddetalleboleta) {
        this.iddetalleboleta = iddetalleboleta;
    }
    
    /*
    public Integer getIdboleta(){
        return this.boleta.getIdboleta();
    }
    */
    public Boleta getBoleta() {
        return boleta;
    }

    public void setBoleta(Boleta boleta) {
        this.boleta = boleta;
    }

    public StockProducto getStockproducto() {
        return stockproducto;
    }

    public void setStockproducto(StockProducto stockproducto) {
        this.stockproducto = stockproducto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getSubtotalneto() {
        return subtotalneto;
    }

    public void setSubtotalneto(Integer subtotalneto) {
        this.subtotalneto = subtotalneto;
    }

    public Integer getSubtotaliva() {
        return subtotaliva;
    }

    public void setSubtotaliva(Integer subtotaliva) {
        this.subtotaliva = subtotaliva;
    }

    public Integer getSubtotalbruto() {
        return subtotalbruto;
    }

    public void setSubtotalbruto(Integer subtotalbruto) {
        this.subtotalbruto = subtotalbruto;
    }

}
