/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "ESTADODETALLEFACTURA")
public class Estadodetallefactura implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDESTADODETALLEFACTURA")
    Integer idestadodetallefactura;
    
    @Column(name = "NOMESTADODETALLEFACTURA")
    String nomestadodetallefactura;

    public Integer getIdestadodetallefactura() {
        return idestadodetallefactura;
    }

    public void setIdestadodetallefactura(Integer idestadodetallefactura) {
        this.idestadodetallefactura = idestadodetallefactura;
    }

    public String getNomestadodetallefactura() {
        return nomestadodetallefactura;
    }

    public void setNomestadodetallefactura(String nomestadodetallefactura) {
        this.nomestadodetallefactura = nomestadodetallefactura;
    }

    public Estadodetallefactura() {
    }
    
    
    
}
