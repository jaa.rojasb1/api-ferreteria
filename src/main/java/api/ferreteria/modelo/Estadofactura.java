
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@ApplicationScoped
@Entity(name = "ESTADOFACTURA")
public class Estadofactura implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDESTADOFACTURA")
    Integer idestadofactura;
    
    @Column(name = "NOESTADOFACTURA")
    String nomestadofactura;

    public Estadofactura() {
    }

    public Estadofactura(Integer idestadofactura, String nomestadofactura) {
        this.idestadofactura = idestadofactura;
        this.nomestadofactura = nomestadofactura;
    }

    public Integer getIdestadofactura() {
        return idestadofactura;
    }

    public void setIdestadofactura(Integer idestadofactura) {
        this.idestadofactura = idestadofactura;
    }

    public String getNomestadofactura() {
        return nomestadofactura;
    }

    public void setNomestadofactura(String nomestadofactura) {
        this.nomestadofactura = nomestadofactura;
    }
    
    
}
