
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@ApplicationScoped
@Entity(name = "ESTADOOC")
public class Estadooc implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDESTADOOC")
    Integer idestadooc;
    
    @Column(name = "NOMESTADOOC")
    String nomestadooc;

    public Estadooc() {
    }

    public Estadooc(Integer idestadooc, String nomestadooc) {
        this.idestadooc = idestadooc;
        this.nomestadooc = nomestadooc;
    }

    public Integer getIdestadooc() {
        return idestadooc;
    }

    public void setIdestadooc(Integer idestadooc) {
        this.idestadooc = idestadooc;
    }

    public String getNomestadooc() {
        return nomestadooc;
    }

    public void setNomestadooc(String nomestadooc) {
        this.nomestadooc = nomestadooc;
    }
    
    
    
}
