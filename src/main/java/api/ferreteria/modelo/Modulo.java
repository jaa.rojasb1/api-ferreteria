package api.ferreteria.modelo;

import java.io.Serializable;
import java.util.Set;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@ApplicationScoped
@Entity(name = "MODULO")
public class Modulo implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDMODULO")
    private int idmodulo;
    
    @Column(name = "NOMMODULO")
    private String nommodulo;

    public Modulo() {
    }

    public Modulo(int idmodulo, String nommodulo) {
        this.idmodulo = idmodulo;
        this.nommodulo = nommodulo;
    }

    public int getIdmodulo() {
        return idmodulo;
    }

    public void setIdmodulo(int idmodulo) {
        this.idmodulo = idmodulo;
    }

    public String getNommodulo() {
        return nommodulo;
    }

    public void setNommodulo(String nommodulo) {
        this.nommodulo = nommodulo;
    }
    
}