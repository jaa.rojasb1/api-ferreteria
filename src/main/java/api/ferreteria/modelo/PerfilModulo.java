
package api.ferreteria.modelo;

import java.io.Serializable;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@ApplicationScoped
@Entity(name = "PERFILMODULO")
public class PerfilModulo implements Serializable{
    /*
    @Id
    @ManyToOne()
    @JoinColumn(name = "IDPERFIL")
    private Perfil perfil;
    
    @Id
    //@ManyToOne()
    @JoinColumn(name = "IDMODULO")
    private Modulo modulo;
    */
    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPERFIL")
    Perfil perfil;
    
    @Id
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDMODULO")
    Modulo modulo;
    
    public PerfilModulo() {
    }

    public PerfilModulo(Perfil Perfil, Modulo Modulo) {
        this.perfil = Perfil;
        this.modulo = Modulo;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil Perfil) {
        this.perfil = Perfil;
    }

    public Modulo getModulo() {
        return modulo;
    }

    public void setModulo(Modulo Modulo) {
        this.modulo = Modulo;
    }

    
}
