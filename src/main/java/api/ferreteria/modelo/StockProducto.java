package api.ferreteria.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@ApplicationScoped
@Entity(name = "STOCKPRODUCTO")
public class StockProducto implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDSTOCKPRODUCTO")
    private Integer idstockproducto;
    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPRODUCTO")
    private Producto producto;
    
    @ManyToOne(optional = false, cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPROVEEDOR")
    private Proveedor proveedor;
    
    @Column(name = "STOCK")
    private Integer stock;
    
    @Column(name = "FECHAVENC")
    LocalDate fechavenc;
    
    @Column(name = "PRECIOVENTA")
    private Integer precioventa;

    public StockProducto() {
    }

    public StockProducto(Integer idstockproducto, Producto producto, Proveedor proveedor, Integer stock, LocalDate fechavenc, Integer precioventa) {
        this.idstockproducto = idstockproducto;
        this.producto = producto;
        this.proveedor = proveedor;
        this.stock = stock;
        this.fechavenc = fechavenc;
        this.precioventa = precioventa;
    }

    public Integer getIdstockproducto() {
        return idstockproducto;
    }

    public void setIdstockproducto(Integer idstockproducto) {
        this.idstockproducto = idstockproducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public LocalDate getFechavenc() {
        return fechavenc;
    }

    public void setFechavenc(LocalDate fechavenc) {
        this.fechavenc = fechavenc;
    }

    public Integer getPrecioventa() {
        return precioventa;
    }

    public void setPrecioventa(Integer precioventa) {
        this.precioventa = precioventa;
    }
    
    
}
