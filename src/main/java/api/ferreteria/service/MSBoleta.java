
import api.ferreteria.modelo.Boleta;
import api.ferreteria.modelo.DetalleBoleta;
import java.util.Collection;
import java.util.Set;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;




@Path("/boleta")
public class MSBoleta{
    
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Boleta> getBoletas(){
        String query = "SELECT B FROM BOLETA B";
        return manager.createQuery(query, Boleta.class).getResultList();

    }
    
    @GET
    @Path("/{idboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Boleta getBoleta(@PathParam("idboleta") Integer idboleta){
        return manager.find(Boleta.class, idboleta);
    }
    
    @GET
    @Path("/cliente/{idcliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Boleta> getBoletasCliente(@PathParam("idcliente") Integer idcliente){
        String query = "SELECT B FROM BOLETA B JOIN B.cliente C WHERE C.idcliente = " + idcliente;
        return manager.createQuery(query, Boleta.class).getResultList();
    }
    
    @GET
    @Path("/{idboleta}/cliente/{idcliente}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<DetalleBoleta> getBoletasCliente(@PathParam("idboleta") Integer idboleta, @PathParam("idcliente") Integer idcliente){
        Boleta b = manager.find(Boleta.class, idboleta);
        Set<DetalleBoleta> detallesBoleta = b.getDetallesboleta();
        return detallesBoleta;
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta postBoleta(Boleta boleta){
        manager.persist(boleta);
        manager.flush(); 
        return manager.find(Boleta.class, boleta.getIdboleta());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta patchBoleta(Boleta boleta){
        manager.merge(boleta);
        manager.flush();
        return manager.find(Boleta.class, boleta.getIdboleta());
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Boleta deleteBoleta(Boleta boleta){
        Boleta f = manager.find(Boleta.class, boleta.getIdboleta());
        manager.remove(f);
        manager.flush();
        return new Boleta();
    }
}
