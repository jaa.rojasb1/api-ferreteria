package api.ferreteria.service;

import api.ferreteria.modelo.Boleta;
import api.ferreteria.modelo.DetalleBoleta;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/detalleboleta")
public class MSDetalleBoleta {

    @Inject
    EntityManager manager;

    @GET
    @Path("/{idboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<DetalleBoleta> getDetallesBoleta(@PathParam("idboleta") Integer idboleta) {
        String query = "SELECT DB FROM BOLETA B JOIN B.detallesboleta DB WHERE B.idboleta = " + idboleta;
        List<DetalleBoleta> detallesboleta = manager.createQuery(query, DetalleBoleta.class).getResultList();
        return detallesboleta;
    }

    @GET
    @Path("/detalleboleta/{iddetalleboleta}")
    @Produces(MediaType.APPLICATION_JSON)
    public DetalleBoleta getDetalleBoleta(@PathParam("iddetalleboleta") Integer iddetalleboleta) {
        return manager.find(DetalleBoleta.class, iddetalleboleta);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta postDetalleBoleta(DetalleBoleta detalleboleta) {
        manager.persist(detalleboleta);
        manager.flush();
        return new DetalleBoleta();
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta patchDetalleBoleta(DetalleBoleta db) {
        DetalleBoleta detalleboleta = manager.find(DetalleBoleta.class, db.getIddetalleboleta());
        detalleboleta.setCantidad(db.getCantidad());
        int precioventa = detalleboleta.getStockproducto().getPrecioventa();
        int cantidad = detalleboleta.getCantidad();
        int neto = cantidad * precioventa;
        int iva = (int)(neto * 1.19) - neto;
        int bruto = neto + iva;
        
        detalleboleta.setSubtotalbruto(bruto);
        detalleboleta.setSubtotaliva(iva);
        detalleboleta.setSubtotalneto(neto);
        
        manager.merge(detalleboleta);
        manager.flush();
        
        int idboleta = detalleboleta.getBoleta().getIdboleta();
        manager.flush();
        
        Boleta b = manager.find(Boleta.class, idboleta);
        
        String query = "SELECT DB FROM BOLETA B JOIN B.detallesboleta DB WHERE B.idboleta = " + b.getIdboleta();
        List<DetalleBoleta> detallesboleta = manager.createQuery(query, DetalleBoleta.class).getResultList();
        
        Integer totalneto = 0;
        Integer totaliva = 0;
        Integer totalbruto = 0;
        
        for(DetalleBoleta dbCuenta : detallesboleta){
            int precio = dbCuenta.getStockproducto().getPrecioventa();
            int cant = dbCuenta.getCantidad();
            
            int n = precio * cant;
            int iv = (int)(n * 1.19) - n;
            int br = n + iv;
            
            totalneto += n;
            totaliva += iv;
            totalbruto += br;
        }
        
        b.setTotalneto(totalneto);
        b.setTotaliva(totaliva);
        b.setTotalbruto(totalbruto);    
        manager.merge(b);
        manager.flush();
        return new DetalleBoleta();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public DetalleBoleta deleteDetalleBoleta(DetalleBoleta db) {        
        DetalleBoleta detalle = manager.find(DetalleBoleta.class, db.getIddetalleboleta());
        
        int idboleta = detalle.getBoleta().getIdboleta();
        
        Boleta b = manager.find(Boleta.class, idboleta);
        manager.remove(detalle);
        manager.flush();
        
        String query = "SELECT DB FROM BOLETA B JOIN B.detallesboleta DB WHERE B.idboleta = " + b.getIdboleta();
        List<DetalleBoleta> detallesboleta = manager.createQuery(query, DetalleBoleta.class).getResultList();
        
        Integer totalneto = 0;
        Integer totaliva = 0;
        Integer totalbruto = 0;
        
        for(DetalleBoleta dbCuenta : detallesboleta){
            int precio = dbCuenta.getStockproducto().getPrecioventa();
            int cant = dbCuenta.getCantidad();
            
            int n = precio * cant;
            int iv = (int)(n * 1.19) - n;
            int br = n + iv;
            
            totalneto += n;
            totaliva += iv;
            totalbruto += br;
        }
        
        b.setTotalneto(totalneto);
        b.setTotaliva(totaliva);
        b.setTotalbruto(totalbruto);    
        manager.merge(b);
        manager.flush();
        
        return new DetalleBoleta();
    }
}
