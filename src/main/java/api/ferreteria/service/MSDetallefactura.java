package api.ferreteria.service;

import api.ferreteria.modelo.Detallefactura;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/detallefactura")
public class MSDetallefactura {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Detallefactura> getDetallesfacturas(){
        String query = "SELECT F FROM ESTADOFACTURA F ORDER BY F.nomdetallefactura ASC";
        return manager.createQuery(query, Detallefactura.class).getResultList();
    }
    
    @GET
    @Path("/{idfactura}")
    @Produces(MediaType.APPLICATION_JSON)
    public Detallefactura getDetallefactura(@PathParam("idfactura") Integer idfactura){
        String query = "SELECT F FROM DETALLEFACTURA F WHERE F.idfactura = " + idfactura;
        List<Detallefactura> detallesfactura = manager.createQuery(query, Detallefactura.class).getResultList();
        if(detallesfactura.size() < 1){
            return new Detallefactura();
        }
        return detallesfactura.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Detallefactura postDetallefactura(Detallefactura detallefactura){
        manager.persist(detallefactura);
        manager.flush(); 
        return manager.find(Detallefactura.class, detallefactura.getIddetallefactura());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Detallefactura patchDetallefactura(Detallefactura detallefactura){
        manager.merge(detallefactura);
        manager.flush();
        return manager.find(Detallefactura.class, detallefactura.getIddetallefactura());
    }
}
