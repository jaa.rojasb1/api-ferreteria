package api.ferreteria.service;

import api.ferreteria.modelo.Empresa;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/empresa")
public class MSEmpresa {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Empresa> getEmpresas(){
        String query = "SELECT F FROM EMPRESA F ORDER BY F.nomempresa ASC";
        return manager.createQuery(query, Empresa.class).getResultList();
    }
    
    @GET
    @Path("/{idempresa}")
    @Produces(MediaType.APPLICATION_JSON)
    public Empresa getEmpresa(@PathParam("idempresa") Integer idempresa){
        String query = "SELECT F FROM EMPRESA F WHERE F.idempresa = " + idempresa;
        List<Empresa> empresas = manager.createQuery(query, Empresa.class).getResultList();
        if(empresas.size() < 1){
            return new Empresa();
        }
        return empresas.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Empresa postEmpresa(Empresa empresa){
        manager.persist(empresa);
        manager.flush(); 
        return manager.find(Empresa.class, empresa.getIdempresa());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Empresa patchEmpresa(Empresa empresa){
        manager.merge(empresa);
        manager.flush();
        return manager.find(Empresa.class, empresa.getIdempresa());
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Empresa deleteEmpresa(Empresa empresa){
        Empresa f = manager.find(Empresa.class, empresa.getIdempresa());
        manager.remove(f);
        manager.flush();
        return new Empresa();
    }
}
