package api.ferreteria.service;

import api.ferreteria.modelo.Estadodetallefactura;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/estadodetallefactura")
public class MSEstadodetallefactura {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Estadodetallefactura> getEstadodetallefacturas(){
        String query = "SELECT F FROM ESTADODETALLEFACTURA F ORDER BY F.nomestadodetallefactura ASC";
        return manager.createQuery(query, Estadodetallefactura.class).getResultList();
    }
    
    @GET
    @Path("/{idfamilia}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estadodetallefactura getEstadodetallefactura(@PathParam("idestadodetallefactura") Integer idestadodetallefactura){
        String query = "SELECT F FROM ESTADODETALLEFACTURA F WHERE F.idestadodetallefactura = " + idestadodetallefactura;
        List<Estadodetallefactura> estadosdetallefactura = manager.createQuery(query, Estadodetallefactura.class).getResultList();
        if(estadosdetallefactura.size() < 1){
            return new Estadodetallefactura();
        }
        return estadosdetallefactura.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadodetallefactura postEstadodetallefactura(Estadodetallefactura estadodetallefactura){
        manager.persist(estadodetallefactura);
        manager.flush(); 
        return manager.find(Estadodetallefactura.class, estadodetallefactura.getIdestadodetallefactura());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadodetallefactura patchEstadodetallefactura(Estadodetallefactura estadodetallefactura){
        manager.merge(estadodetallefactura);
        manager.flush();
        return manager.find(Estadodetallefactura.class, estadodetallefactura.getIdestadodetallefactura());
    }
}

