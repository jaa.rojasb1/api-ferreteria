package api.ferreteria.service;

import api.ferreteria.modelo.Estadofactura;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/estadofactura")
public class MSEstadofactura {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Estadofactura> getEstadosfactura(){
        String query = "SELECT F FROM ESTADOFACTURA F ORDER BY F.nomestadofactura ASC";
        return manager.createQuery(query, Estadofactura.class).getResultList();
    }
    
    @GET
    @Path("/{idestadofactura}")
    @Produces(MediaType.APPLICATION_JSON)
    public Estadofactura getEstadofactura(@PathParam("idestadofactura") Integer idestadofactura){
        String query = "SELECT F FROM ESTADOFACTURA F WHERE F.idestadofactura = " + idestadofactura;
        List<Estadofactura> estadosfactura = manager.createQuery(query, Estadofactura.class).getResultList();
        if(estadosfactura.size() < 1){
            return new Estadofactura();
        }
        return estadosfactura.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadofactura postEstadofactura(Estadofactura estadofactura){
        manager.persist(estadofactura);
        manager.flush(); 
        return manager.find(Estadofactura.class, estadofactura.getIdestadofactura());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Estadofactura patchEstadofactura(Estadofactura estadofactura){
        manager.merge(estadofactura);
        manager.flush();
        return manager.find(Estadofactura.class, estadofactura.getIdestadofactura());
    }
}
