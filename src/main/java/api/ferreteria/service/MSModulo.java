package api.ferreteria.service;

import api.ferreteria.modelo.Modulo;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/modulo")
public class MSModulo {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Modulo> getModulos(){
        String query = "SELECT C FROM MODULO C ORDER BY C.nommodulo ASC"; //Query JPQL para traer todos los cargos.
        return manager.createQuery(query, Modulo.class).getResultList(); //El EntityManager inyectado al comienzo se encarga del manejo de las clases.
    }
    
    @GET
    @Path("/{idmodulo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Modulo getModulo(@PathParam("idmodulo") Integer idmodulo){
        String query = "SELECT F FROM MODULO F WHERE F.idmodulo = " + idmodulo;
        List<Modulo> modulos = manager.createQuery(query, Modulo.class).getResultList();
        if(modulos.size() < 1){
            return new Modulo();
        }
        return modulos.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Modulo postModulo(Modulo modulo){
        manager.persist(modulo);
        manager.flush(); 
        return manager.find(Modulo.class, modulo.getIdmodulo());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Modulo patchModulo(Modulo modulo){
        manager.merge(modulo);
        manager.flush();
        return manager.find(Modulo.class, modulo.getIdmodulo());
    }
    
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Modulo deleteModulo(Modulo modulo){
        Modulo f = manager.find(Modulo.class, modulo.getIdmodulo());
        manager.remove(f);
        manager.flush();
        return new Modulo();
    }
}
