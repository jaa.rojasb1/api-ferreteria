package api.ferreteria.service;

import api.ferreteria.modelo.Modulo;
import api.ferreteria.modelo.PerfilModulo;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import javax.ws.rs.GET;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/perfilmodulo")
public class MSPerfilModulo {

    @Inject EntityManager manager;
    

    @GET 
    @Produces(MediaType.APPLICATION_JSON) 
    public Collection<PerfilModulo> getPerfilModulos(){
        String query = "SELECT PM FROM PERFILMODULO PM JOIN PM.modulo M JOIN PM.perfil P ORDER BY M.nommodulo ASC, P.nomperfil ASC"; 
        return manager.createQuery(query, PerfilModulo.class).getResultList();
    }

    @GET
    @Path("/{idperfil}") 
    @Produces(MediaType.APPLICATION_JSON) 
    public Collection<Modulo> getPerfilModulo(@PathParam("idperfil") Integer idperfil){
        String query = "SELECT M FROM PERFILMODULO PM JOIN PM.modulo M JOIN PM.perfil P WHERE P.idperfil = " + idperfil + " ORDER BY M.nommodulo ASC, P.nomperfil ASC"; 
        List<Modulo> modulos = manager.createQuery(query, Modulo.class).getResultList(); 
        if(modulos.size() < 1){ 
            return new LinkedList<Modulo>();
        }
        return modulos; 
    }
    
}
