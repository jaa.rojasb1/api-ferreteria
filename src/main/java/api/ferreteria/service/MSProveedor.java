package api.ferreteria.service;

import api.ferreteria.modelo.Proveedor;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/proveedor")
public class MSProveedor {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Proveedor> getProveedores(){
        String query = "SELECT F FROM PROVEEDOR F ORDER BY F.nomproveedor ASC";
        return manager.createQuery(query, Proveedor.class).getResultList();
    }
    
    @GET
    @Path("/{idproveedor}")
    @Produces(MediaType.APPLICATION_JSON)
    public Proveedor getProveedor(@PathParam("idproveedor") Integer idproveedor){
        String query = "SELECT F FROM PROVEEDOR F WHERE F.idproveedor = " + idproveedor;
        List<Proveedor> proveedores = manager.createQuery(query, Proveedor.class).getResultList();
        if(proveedores.size() < 1){
            return new Proveedor();
        }
        return proveedores.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Proveedor postProveedor(Proveedor proveedor){
        manager.persist(proveedor);
        manager.flush(); 
        return manager.find(Proveedor.class, proveedor.getIdproveedor());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Proveedor patchProveedor(Proveedor proveedor){
        manager.merge(proveedor);
        manager.flush();
        return manager.find(Proveedor.class, proveedor.getIdproveedor());
    }
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Proveedor deleteProveedor(Proveedor proveedor){
        Proveedor f = manager.find(Proveedor.class, proveedor.getIdproveedor());
        manager.remove(f);
        manager.flush();
        return new Proveedor();
    }
}
