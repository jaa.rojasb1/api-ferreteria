package api.ferreteria.service;

import api.ferreteria.modelo.StockProducto;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/stockproducto")
public class MSStockProducto {
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<StockProducto> getStockProductos(){
        String query = "SELECT SP FROM STOCKPRODUCTO SP";
        return manager.createQuery(query, StockProducto.class).getResultList();
    }
    
    @GET
    @Path("/{idstockproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public StockProducto getStockProducto(@PathParam("idstockproducto") Integer idstockproducto){
        String query = "SELECT SP FROM STOCKPRODUCTO SP WHERE SP.idstockproducto = " + idstockproducto;
        List<StockProducto> stockproductos = manager.createQuery(query, StockProducto.class).getResultList();
        if(stockproductos.size() < 1){
            return new StockProducto();
        }
        return stockproductos.get(0);
    }
    
    @GET
    @Path("/producto/{idproducto}")
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<StockProducto> getStockProductoIdProducto(@PathParam("idproducto") Integer idproducto){
        String query = "SELECT SP FROM STOCKPRODUCTO SP JOIN SP.producto P WHERE P.idproducto = " + idproducto;
        return manager.createQuery(query, StockProducto.class).getResultList();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public StockProducto postStockProducto(StockProducto stockproducto){
        manager.persist(stockproducto);
        manager.flush();
        return manager.find(StockProducto.class, stockproducto.getIdstockproducto());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public StockProducto patchStockProducto(StockProducto stockproducto){
        manager.merge(stockproducto);
        manager.flush();
        return manager.find(StockProducto.class, stockproducto.getIdstockproducto());
    }
    
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public StockProducto deleteStockProducto(StockProducto stockproducto){
        StockProducto sp = manager.find(StockProducto.class, stockproducto.getIdstockproducto());
        manager.remove(sp);
        manager.flush();
        return new StockProducto();
    }
}
