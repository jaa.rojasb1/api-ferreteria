package api.ferreteria.service;

import api.ferreteria.com.Util;
import api.ferreteria.modelo.Cargo;
import api.ferreteria.modelo.Usuario;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/usuario")
public class MSUsuario {

    @Inject
    EntityManager manager;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Usuario> getUsuarios() {
        String query = "SELECT U FROM USUARIO U";
        List<Usuario> usuarios = manager.createQuery(query, Usuario.class).getResultList();
        return usuarios;
    }

    @GET
    @Path("/{idusuario}")
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario getUsuario(@PathParam("idusuario") Integer idusuario) {
        String query = "SELECT U FROM USUARIO U WHERE U.idusuario = " + idusuario;
        Usuario u = manager.createQuery(query, Usuario.class).getResultList().get(0);
        return u;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Usuario postUsuario(Usuario usuario) {
        manager.persist(usuario);
        manager.flush();
        return manager.find(Usuario.class, usuario.getIdusuario());
    }

    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Usuario patchUsuario(Usuario usuario) {
        manager.merge(usuario);
        manager.flush();
        return manager.find(Usuario.class, usuario.getIdusuario());
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Usuario deleteUsuario(Usuario usuario) {
        Usuario u = manager.find(Usuario.class, usuario.getIdusuario());
        manager.remove(u);
        manager.flush();
        return new Usuario();
    }
}
