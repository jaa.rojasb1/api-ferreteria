package api.ferreteria.service;

import api.ferreteria.modelo.Familia;
import api.ferreteria.modelo.Valorunitario;
import java.util.Collection;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/valorunitario")
public class MSValorunitario {
    
    @Inject EntityManager manager;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Valorunitario> getValoresunitarios(){
        String query = "SELECT F FROM VALORUNITARIO F ORDER BY F.cantmoneda ASC";
        return manager.createQuery(query, Valorunitario.class).getResultList();
    }
    
    @GET
    @Path("/{idmoneda}")
    @Produces(MediaType.APPLICATION_JSON)
    public Valorunitario getValorunitario(@PathParam("idvalorunitario") Integer idvalorunitario){
        String query = "SELECT F FROM MONEDA F WHERE F.idmoneda = " + idvalorunitario;
        List<Valorunitario> valoresunitarios = manager.createQuery(query, Valorunitario.class).getResultList();
        if(valoresunitarios.size() < 1){
            return new Valorunitario();
        }
        return valoresunitarios.get(0); 
    }
    
    @POST 
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Familia postFamilia(Familia familia){
        manager.persist(familia);
        manager.flush(); 
        return manager.find(Familia.class, familia.getIdfamilia());
    }
    
    @PATCH
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Familia patchFamilia(Familia familia){
        manager.merge(familia);
        manager.flush();
        return manager.find(Familia.class, familia.getIdfamilia());
    }
}
